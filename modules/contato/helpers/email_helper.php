<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function enviar_email($para, $assunto, $corpo, $remetente = '', $copia_remetente = false, $arquivo = null, $arquivo_nome = '')
{
    $CI =& get_instance();

    $CI->load->library('contato/F_PHPMailer');

    $mail = new PHPMailer();
//    $mail->SMTPDebug  = 2;
    $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
    $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
    $mail->Host = $CI->config->item('smtp')['host']; //smtp
    $mail->Port = $CI->config->item('smtp')['porta']; //Estabelecemos a porta utilizada pelo servidor de email.
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
    $mail->Username = $CI->config->item('smtp')['usuario']; //Usuário do gMail
    $mail->Password = $CI->config->item('smtp')['senha']; //Senha do gMail

    if($remetente == '')
        $remetente = $CI->config->item('smtp')['remetente'];


        $mail->SetFrom($remetente, '=?UTF-8?B?' . base64_encode('Zero Defect') . '?='); //Quem está enviando o e-mail.
        $mail->AddReplyTo($remetente, '=?UTF-8?B?' . base64_encode('Zero Defect') . '?='); //Para que a resposta será enviada.

        $mail->CharSet = "ISO-8859-1";
        $mail->Subject = '=?UTF-8?B?' . base64_encode($assunto) . '?='; //Assunto do e-mail.

        if (!is_null($arquivo))
            $mail->addAttachment($arquivo, utf8_decode($arquivo_nome));


    //    $mail->AddBCC($_SESSION['filial']['email_gerente'], $assunto); // Cópia Oculta
        $mail->AddBCC('assistenteweb@singulo.com.br', $assunto); // Cópia Oculta
        $mail->AddBCC('cristiane.pacheco@zero-defect.com.br', $assunto); // Cópia Ocult

        if ($copia_remetente)
            $mail->AddBCC($remetente, 'Cópia oculta - ' . $assunto);

        $mail->Body = utf8_decode($corpo);
        $mail->AddAddress($para);
        return $mail->Send();
}