$(window).load(function() {
    // Animate loader off screen
    $(".loading").fadeOut("slow");
});
var herois= {};
var codigo_resposta;

$(document).ready(function() {
    $("#form-pergunta").steps({
        headerTag: 'h1',
        bodyTag: 'section',
        transitionEffect: "fade",
        autoFocus: true,
    });

    $("#form-pergunta input[type=radio]").change(function() {
        if($(this).data('ultimo'))
            gerar_resposta();
        else
            $("#form-pergunta").steps("next");
    });

     herois = {
        AAAAA:'homem_de_ferro',
        AAAAB:'homem_de_ferro',
        AAABB:'homem_de_ferro',
        AABBB:'flash',
        ABBBB:'homem_aranha',
        ABBBA:'homem_aranha',
        ABBAA:'mulher_maravilha',
        ABAAA:'thor',
        ABABA:'flash',
        AABAB:'mulher_maravilha',
        AAABA:'Loki',
        AABBA:'Loki',
        ABABB:'Loki',
        ABBAB:'mulher_maravilha',
        ABAAB:'thor',
        AABAA:'mulher_maravilha',
        BBBBB:'homem_aranha',
        BBBBA:'flash',
        BBBAA:'thor',
        BBAAA:'thor',
        BAAAA:'homem_de_ferro',
        BAAAB:'homem_de_ferro',
        BAABB:'Loki',
        BABBB:'homem_aranha',
        BABAB:'mulher_maravilha',
        BBABA:'flash',
        BAABA:'Loki',
        BABBA:'flash',
        BBABB:'flash',
        BBBAB:'thor',
        BBAAB:'thor',
        BABAA:'homem_aranha'
    }

});

function gerar_resposta()
{
    var respostas = $("#form-pergunta").jsonify();
    codigo_resposta = (''+  respostas['pergunta_1'] + respostas['pergunta_2'] + respostas['pergunta_2'] + respostas['pergunta_3'] + respostas['pergunta_4']);

    if(herois[codigo_resposta] != undefined)
        window.location.href = $('#base_url').val() + 'jogo/resultado/' + herois[codigo_resposta].toLowerCase();
    else
        window.location.href = $('#base_url').val() + 'jogo/resultado/mistica';
}
