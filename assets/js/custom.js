$(window).load(function() {
    // Animate loader off screen
    $(".loading").fadeOut("slow");
});


$(document).ready(function() {
    $.mask.definitions['~'] = '([0-9] )?';
    $('.telefone').mask('(99)9999-9999~');
});

var owlServicos = $('.lista-servicos .servicos').owlCarousel({
    nav: false,
    dots: true,
    autoplay: false,
    loop: false,
    navText: [""],
    responsiveClass: true,
    center: true,
    items: 1,
    responsive: {
        320: {
            margin: 50,
        },
        1000:{
            margin: 15,
        }
    }
});

var owlClientes = $('.lista-clientes .owl-clientes').owlCarousel({
    nav:false,
    dots: false,
    autoplay:true,
    loop: true,
    navText: [""],
    responsiveClass:true,
    autoplayTimeout:800,
    smartSpeed: 250,
    margin: 20,
    responsive:{
        320:{
            items: 3,
            margin: 20,
            nav: false
        },
        1000:{
            items: 3,
        }
    }
});

owlServicos.on('changed.owl.carousel', function(event) {
     if(event.item.index + 1 > 1)
     {
         $('.owl-controle.prev')[0].style.cursor = "pointer";
         $('.owl-controle.prev')[0].disabled = false;
     }
     else
     {
         $('.owl-controle.prev')[0].style.cursor = null;
         $('.owl-controle.prev')[0].disabled = true;
     }

    if(event.item.index + 1 == $('.owl-item .servico').length)
    {
        $('.owl-controle.next')[0].style.cursor = "not-allowed";
        $('.owl-controle.next')[0].disabled = true;
    }
    else
    {
        $('.owl-controle.next')[0].style.cursor = null;
        $('.owl-controle.next')[0].disabled = false;
    }
});

$.validator.messages.required = 'Campo requerido!';
$.validator.messages.require_from_group = 'Por favor preencha pelo menos um telefone.';
$.validator.messages.equalTo = 'Os campos de senha devem ser iguais!';
$.validator.messages.minlength = 'Por favor preencha com no mínimo {0} characters.';
$.validator.messages.email = 'Por favor digite um email valido.';

alertify.logPosition("bottom right");

function enviar_contato()
{
    if( ! $('#form-contato').valid())
        return;

    $('#form-contato button').button('loading');

    var contato =  $('#form-contato').jsonify();

    $.ajax({
        url : $('#base_url').val() + 'contato/novo',
        type : 'POST',
        dataType: "json",
        data: { contato: contato},
        success : function(data) {

            if(data.status)
            {
                alertify.success('Contato enviado com sucesso!');
                document.getElementById('form-contato').reset();
            }
            else
                alertify.error('Ocorreu um erro ao enviar seu contato, por favor tente mais tarde.');
        },
        error : function(request, error)
        {
            alertify.error('Ocorreu um erro ao enviar seu contato.');
            console.log('Erro');
            console.log(request);
            console.log(error);
        },
        complete: function (data) {
            $('#form-contato button').button('reset')
        }
    });
}


function enviar_trabalhe_conosco()
{
    if( ! $('#form-trabalhe-conosco').valid())
        return;

    $('#form-trabalhe-conosco button').button('loading');

    var contato =  new FormData($('#form-trabalhe-conosco')[0]);

    $.ajax({
        url : $('#base_url').val() + 'contato/novo_curriculo',
        type : 'POST',
        dataType: "json",
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        data: contato,
        success : function(data) {

            if(data.status)
            {
                alertify.success('Currículo enviado com sucesso, por favor aguarde nosso retorno.');
                document.getElementById('form-trabalhe-conosco').reset();
            }
            else if(data.msg != undefined)
                alertify.error(data.msg);
            else
                alertify.error('Ocorreu uma falha ao enviar seu contato, por favor tente mais tarde.');
        },
        error : function(request, error)
        {
            alertify.error('Ocorreu um erro ao enviar seu contato.');
            console.log('Erro');
            console.log(request);
            console.log(error);
        },
        complete: function (data) {
            $('#form-trabalhe-conosco button').button('reset')
        }
    });
}


