var owlDiretores = $('.diretores .lista-diretores').owlCarousel({
    nav: false,
    dots: false,
    autoplay: false,
    loop: false,
    navText: [""],
    responsiveClass: true,
    center: true,
    margin:15,
    items: 1
});

owlDiretores.on('changed.owl.carousel', function(event) {
    if(event.item.index + 1 > 1)
    {
        $('.diretores .owl-controle.prev')[0].style.cursor = "pointer";
        $('.diretores .owl-controle.prev')[0].disabled = false;
    }
    else
    {
        $('.diretores .owl-controle.prev')[0].style.cursor = null;
        $('.diretores .owl-controle.prev')[0].disabled = true;
    }

    if(event.item.index + 1 == $('.owl-item .diretor').length)
    {
        $('.diretores .owl-controle.next')[0].style.cursor = "not-allowed";
        $('.diretores .owl-controle.next')[0].disabled = true;
    }
    else
    {
        $('.diretores .owl-controle.next')[0].style.cursor = null;
        $('.diretores .owl-controle.next')[0].disabled = false;
    }
});

$('.menu .nav.navbar-nav li').on('click', function(){
    $('.navbar-collapse.collapse').collapse('toggle');
});

$(document).ready(function() {
    $('#mobile-fullpage').fullpage({

        scrollingSpeed: 1500,
        controlArrows: false,
        anchors: ['home', 'azero', 'solucoes', 'trabalhe-conosco', 'equipe', 'clientes', 'fale-conosco', 'endereco'],
        scrollOverflow: false,
        fitToSection: false,
        scrollBar: true,
        autoScrolling: false,
        'onLeave': function (index, nextIndex, direction) {
            if (index == 1 && direction == 'down') {
                $('.titulo-home').addClass('move-down');
            }
            else if (index == 2 && direction == 'up' || nextIndex == 1) {
                $('.titulo-home').removeClass('move-down');
            }
        },

        afterLoad: function (anchorLink, index) {
            //using index
            if (index == 1) {
                $('.titulo-home').removeClass('move-down');
            }
        }
    });
});