<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function index()
    {
        if(! $this->config->item('maintenance') || $_SERVER['REMOTE_ADDR'] == '201.21.60.80')
        {
            $this->load->helper('texto-helper_helper');
            $this->load->helper('colaboradores-helper_helper');

            $data['colaboradores'] = buscar_fotos_colaboradores();

            $data['textos'] = getTextos();

            if($this->isMobile())
                $this->load->view('mobile',$data);
            else
                $this->load->view('home', $data);
        }
        else
        {
            $this->load->view('errors/html/error_maintenance');
        }
    }

    private function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

}