<?php

class Jogo extends  CI_Controller
{
    public  function  index()
    {
        $this->load->helper('perguntas-helper_helper');

        $data['perguntas'] = getPerguntas();

        $this->load->view('jogo/formulario',$data);
    }

    public function resultado($heroi)
    {

        if(($_SERVER['HTTP_USER_AGENT'] == 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)') || ($_SERVER['HTTP_REFERER'] == 'http://www.zero-defect.com.br/jogo') || ($_SERVER['HTTP_REFERER'] == 'http://www.zero-defect.com.br/jogo/'))
        {
            $data['heroi'] = $heroi;
            $this->load->view('jogo/resultado',$data);
        }
        else
        {
            redirect('jogo');
        }
    }
}