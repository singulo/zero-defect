<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '../modules/contato/helpers/email_helper.php';

class Contato extends CI_Controller
{
    public function novo()
    {
        $conteudo = '';
        foreach($_POST['contato'] as $key => $value)
        {
            $conteudo .= '<b>' . str_replace('_', ' ', $key) . '</b>: ';

            if(is_array($value))
                $conteudo .= implode(', ', $value) . '<br />';
            else
                $conteudo .= $value . '<br />';
        }

        $resposta['status'] = enviar_email('zero-defect@zero-defect.com.br',
            'Novo contato enviado através do site', $conteudo);

        echo json_encode($resposta);
    }


    public function novo_curriculo()
    {
        if ( ! isset($_FILES['arquivo']) || $_FILES['arquivo']['size'] > $this->config->item('arquivo_email')['tamanho_maximo'])
        {
            $resposta['msg'] = 'Arquivo acima do limite de ' . $this->formatSizeUnits($this->config->item('arquivo_email')['tamanho_maximo']);
            $resposta['status'] = false;
            echo json_encode($resposta);
            return;
        }

        $contato = $_POST;

        $conteudo = '';
        foreach($contato as $key => $value)
        {
            $conteudo .= '<b>' . str_replace('_', ' ', $key) . '</b>: ';

            if(is_array($value))
                $conteudo .= implode(', ', $value) . '<br />';
            else
                $conteudo .= $value . '<br />';
        }

        $resposta['status'] = enviar_email('querotrabralharn@zero-defect.com.br',
            'Novo currículo enviado através do site', $conteudo, null, false, $_FILES['arquivo']['tmp_name'], $_FILES['arquivo']['name']);

        echo json_encode($resposta);
    }


    private function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

}