<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class maintenance
{
    var $CI;
    public function maintenance()
    {
        $CI = get_instance();

        //var_dump($_SERVER['REMOTE_ADDR']); die;

        if($CI->config->item('maintenance') && $_SERVER['REMOTE_ADDR'] != '201.21.60.80')
        {
            $_error =& load_class('Exceptions', 'core');
            echo $_error->show_error("", "", 'error_maintenance', 200);
            exit;
        }
    }
}