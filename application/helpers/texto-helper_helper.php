<?php

function getTextos()
{
    $textos = array();
    $textos['servicos'] = [
        [
            'titulo'        => 'NA RUA, NA CHUVA <br>OU NA FAZENDA',
            'tipo'          => 'TESTE DE MOBILE',
            'descricao'     => 'Temos uma bancada recheada de dispositivos móveis que podem simular vários tipos de usuários com seus celulares. É a garantia de que o sistema será visto de forma correta, seja pelos que gostam da Lenovo, Axon e até pelos AppleManíacos.',
            'img'           => base_url('assets/images/teste-mobile.jpg'),
            'img_attr_alt'  => 'Teste para mobile',
        ],
        [
            'titulo'        => 'PRA QUEM SABE<br>VENDER O SEU PEIXE',
            'tipo'          => 'TESTE DE PERFORMANCE',
            'descricao'     => 'O que acontece quando você vende algo muito legal e todo mundo quer? Filas e mais filas… Faça a simulação de uma demanda grande de usuários no seu sistema, identifique os gargalos do seu e-commerce e evite um desgaste com seus clientes.',
            'img'           => base_url('assets/images/teste-performance.jpg'),
            'img_attr_alt'  => 'Teste de performance',
        ],
        [
            'titulo'        => 'SEM DOR, SEM <br> GANHO, MONSTRÃO',
            'tipo'          => 'TESTE DE E-COMMERCE',
            'descricao'     => 'Nós garantimos o bom funcionamento da sua loja, para isso somamos testes funcionais, de performance e de segurança. Com isso autenticamos que a sua loja aguenta um grande número de usuários e não será invadida, garantindo a segurança dos dados dos seus clientes, tudo isso para deixá-los felizes e prontos para indicar seu negócio com uma ótima experiência de compra.',
            'img'           => base_url('assets/images/teste-e-commerce.jpg'),
            'img_attr_alt'  => 'Teste de e-commerce',
        ],
        [
            'titulo'        => 'CUIDADO COM <br>O CRACKER',
            'tipo'          => 'TESTE DE SEGURANÇA',
            'descricao'     => 'Em um mundo cada vez mais ameaçado por pragas virtuais o teste de segurança é feito para evitar uma invasão por estranhos, a Zero-Defect sabe como apontar as vulnerabilidades dos sistemas e ambientes. Será que você está preparado com suas defesas? E as atualizações, tem feito? Fale com a gente.',
            'img'           => base_url('assets/images/teste-segurança.jpg'),
            'img_attr_alt'  => 'Teste de segurança',
        ],
    ];
    $textos['diretores'] = [
        ['nome' => 'CAREN GARAVELLO','descricao' => 'Números e as mais complexas planilhas fazem parte do trabalho da Caren. Formada em Administração pela PUCRS, esta sócia manda bem em organização e eficiência. Sua função é planejar e manter as finanças da empresa.', 'img' => base_url('assets/images/caren.jpg')],
//        ['nome' => 'DIEGO OLIVEIRA',  'descricao' => 'Entre solos elaborados e baquetadas na testa, este sócio é formado em Ciências da computação pela PUCRS e pós-graduado pela FGV em gerência de projetos. O Diego nos ajuda ordenar o time para que todos sigam a mesma batida.', 'img' => base_url('assets/images/diego.jpg')],
        ['nome' => 'FABIO VALTER SISCATE', 'descricao' => 'Formado em Ciências da computação e pós-graduado em gestão de serviços de TI. O Fábio também curte velocidade e por isso ele é o sócio responsável por todos os testes de performance da empresa.', 'img' => base_url('assets/images/fabio.jpg')],
        ['nome' => 'RAFAEL KRUG',  'descricao' => 'Este sócio é formado em Administração de empresas com ênfase em análise de sistemas pela PUCRS, e pós graduado em gestão empresarial pela ESPM. O Rafael é multitarefas, ágil, faixa preta em Karatê e o objetivo dele é levar a Zero-Defect à perfeição.','img' => base_url('assets/images/rafael.jpg')],
    ];

    return $textos;
}