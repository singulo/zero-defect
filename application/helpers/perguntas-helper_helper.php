<?php

function getPerguntas()
{
    return [
        ['nome' => 'pergunta_1', 'pergunta' => 'Quanto tempo você consegue ficar esperando uma tela responder?', 'alternativas' => [ 'A' => '3 segundos',  'B' => '1 minuto' ]],
        ['nome' => 'pergunta_2', 'pergunta' => 'Com quantos clicks você consegue fechar um processo de compra?', 'alternativas' => [ 'A' => 'Poucos clicks',  'B' => 'Muitos clicks' ]],
        ['nome' => 'pergunta_3', 'pergunta' => 'Para quem você costuma comprar na internet?', 'alternativas' => [ 'A' => 'Para você mesmo',  'B' => 'Para amigos e família' ]],
        ['nome' => 'pergunta_4', 'pergunta' => 'O que você prefere comprar na internet?', 'alternativas' => [ 'A' => 'Filmes, músicas e livros',  'B' => 'Artigos esportivos' ]],
        ['nome' => 'pergunta_5', 'pergunta' => 'Que tipo de publicidade te chama para e-commerce?', 'alternativas' => [ 'A' => 'Engraçada',  'B' => 'Objetiva' ], 'ultima-pergunta' => true]
    ];
}
