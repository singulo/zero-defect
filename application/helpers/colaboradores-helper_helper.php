<?php

function buscar_fotos_colaboradores()
{
    $colaboradores = [];
    foreach(array_diff(scandir(APPPATH .'../assets/images/colaboradores'), ['.','..']) as $colaborador)
    {
        $_colaborador = new stdClass();
        $_colaborador->foto = $colaborador;

        $colaborador = explode('_',str_replace('.jpg','',$colaborador));

        $_colaborador->nome = $colaborador[0];
        $_colaborador->sobrenome = preg_replace('/[0-9]+/', '', $colaborador[1]);

        $colaboradores[] = $_colaborador;
    }

    return $colaboradores;
}


