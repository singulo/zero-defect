<?php $this->load->view('templates/head', ['appendStyles' => [
    base_url('assets/css/glitch.css'),
    base_url('assets/css/desktop.css')
]]); ?>

<?php
$dados = $this->config->item('zero-defect');
?>

<div class="loading" style="background: url(<?= base_url('assets/images/loading.gif');?>) center no-repeat #fff;" ></div>

<!--Menu-->
<div class="menu-container">
    <div class="container">
        <!-- Static navbar -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><img src="<?php echo base_url('assets/images/logo.jpg'); ?>" alt="Logo Zero-Defect"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#azero">A Zero Defect</a></li>
                        <li><a href="#solucoes" tabindex="2">Soluções</a></li>
                        <!--<li><a href="#trabalhe-conosco/0">TRABALHE CONOSCO</a></li>
                        <li><a href="#equipe/0">EQUIPE</a></li>-->
                        <li><a href="#clientes" onclick="fbq('track', 'viuClientes');" tabindex="3">Clientes</a></li>
                        <li><a href="#fale-conosco" tabindex="4">Contato</a></li>
                        <li><a href="#endereco" tabindex="5">Onde estamos</a></li>
                        <li>
                            <a href="https://www.facebook.com/zdefect/" target="_blank" tabindex="6">
                                <span class="fa-stack">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/zerodefecttesthouse/" target="_blank" tabindex="7">
                                <span class="fa-stack">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
    </div>
</div>

<!--Home-->
<div id="fullpage">
    <div class="section home" style="background-image: url(<?= base_url('assets/images/home.jpg'); ?>); background-position: center;background-size: cover;">
        <div class="container">
            <div class="col-md-12">
                <div class="home row">
                    <div class="col-md-offset-3 col-md-6 titulo-home">
                        <p>NÓS SOMOS <br><h1>A ZERO DEFECT</h1></p>
                        <a href="#azero" class="ir-para-conteudo" tabindex="1">
                            Ir para o conteúdo
                            <br>
                            <i class="fa fa-arrow-down"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--Sobre-->
    <div class="section sobre">
        <div class="container">
            <div class="col-md-offset-4 col-md-6">
                <div class="row pull-left">
                    <div class="glitch" data-text="Defect">Zero</div>
                </div>
<!--                <img src="--><?//= base_url('assets/images/loading.gif')?><!--" class="img-responsive" style="height: 100px; margin-top: 25px; float: right;">-->
            </div>
            <div class="col-md-offset-4 col-md-6 text-center sobre-zero">
                <div class="row">
                    <p>Nós somos a Zero-Defect Test House. Estamos aqui por que acreditamos que podemos ir mais além. Um grupo de jovens cansados da mesmice focado em mudar o mundo oferecendo uma forma de garantir que os softwares dos nossos clientes sejam mais confiáveis e competitivos.</p>
                </div>
            </div>
            <div class="col-md-offset-4 col-md-8">
                <div class="row">
                    <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fzdefect%2Fvideos%2F694452007411694&width=570&show_text=false&height=320&appId" width="570" height="320" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--Serviços-->
    <div class="section lista-servicos">
        <div class="container">
            <div class="col-md-2">
                <button type="button" class="owl-controle prev pull-right" onclick="owlServicos.trigger('prev.owl.carousel');" disabled>
                    <img src="<?php echo base_url('assets/images/back.png')?>" alt="" class="img-responsive  ">
                </button>
            </div>
            <div class="col-md-8">
                <div class="col-md-12 servicos">
                    <?php foreach($textos['servicos'] as $servico) : ?>
                        <div class="item servico">
                            <img src="<?= $servico['img']; ?>" alt="<?= $servico['img_attr_alt']; ?>" class="img-responsive center-block">
                            <h2 class="text-center titulo"><?= $servico['tipo']; ?></h2>
<!--                            <p class="tipo-servico text-center">--><?//= $servico['tipo']; ?><!--</p>-->
                            <p class="descricao-servico text-justify center-block"><?= $servico['descricao']; ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-md-2">
                <button type="button" class="owl-controle next" onclick="owlServicos.trigger('next.owl.carousel');">
                    <img src="<?php echo base_url('assets/images/next.png')?>" alt="" class="img-responsive">
                </button>
            </div>
        </div>
    </div>
<!--Trabalhe conosco-->
    <div class="section trabalhe-conosco">
        <div class="slide" style="background-image: url(<?php echo  base_url('assets/images/trabalhe-conosco.jpg'); ?>); background-position: center;background-size: cover;">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 titulo-trabalhe-conosco">
                            <p>NÓS <br>ESTAMOS <br>BUSCANDO <br>O TALENTO <br>QUE HÁ <br>EM VOCÊ</p>
                        </div>
                        <div class="col-md-3 link-form">
                            <a onclick="$.fn.fullpage.moveSlideRight();">
                                <p>TRABALHE COM A GENTE   <img src="<?php echo  base_url('assets/images/next-branco.png')?>" alt=""></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide form-trabalhe-conosco" style="background-image: url(<?php echo base_url('assets/images/form-trabalhe-conosco.jpg'); ?>); background-position: center;background-size: cover;">
            <div class="col-md-offset-2 col-md-4 titulo-form-trabalhe-conosco">
                <p>SE VOCÊ É<br> UM JIRAYA<br> OU UMA<br> MAMBA<br> NEGRA<br> NO QUE FAZ,<br> FALE COM<br> A GENTE:</p>
            </div>
            <div class="col-md-offset-1 col-md-4 form-curriculo">
                <div class="row">
                    <?php $this->load->view('templates/contato/form-trabalhe-conosco'); ?>
                </div>
            </div>
        </div>
    </div>
<!--Diretores-->
    <div class="section diretores">
        <div class="slide">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-offset-3 col-lg-offset-0 col-md-6 titulo-diretores">
                            <p style="margin-bottom: 25px;">NOSSOS DIRETORES</p>
                        </div>
                        <div class="col-md-12 lista-diretores">
                            <?php foreach($textos['diretores'] as $diretor) : ?>
                                <div class="col-md-4 diretor">
                                    <img src="<?= $diretor['img']; ?>" class="img-responsive img-diretor center-block" alt="<?= $diretor['nome']; ?>">
                                    <p class="nome text-center"><?= $diretor['nome']; ?></p>
                                    <p class=" descricao text-justify center-block"><?= $diretor['descricao']; ?></p>
                                </div>
                            <?php endforeach; ?>
                        </div>
<!--                        <div class="col-md-offset-4 col-md-6">
                            <button class="button button--saqui button--inverted button--text-thick button--text-upper button--size-s  ver-colaboradores center-block" data-text="CONHEÇA O DREAM TEAM DA ZERO" onclick="$.fn.fullpage.moveSlideRight();">CONHEÇA O DREAM TEAM DA ZERO</button>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="slide colaboradores">
            <div class="col-md-12">
                <div class="row">
                    <?php /*foreach($colaboradores as $colaborador) : */?>
                        <div class="col-md-2 colaborador">
                            <div class="view view-tenth">
                                <img src="<?/*= base_url('assets/images/colaboradores/'.$colaborador->foto); */?>">
                                <div class="mask">
                                    <h2><?/*= $colaborador->nome; */?></h2>
                                    <p><?/*= $colaborador->sobrenome; */?></p>
                                </div>
                            </div>
                        </div>
                    <?php /*endforeach; */?>
                </div>
            </div>
        </div>-->
    </div>
<!--Clientes-->
    <div class="section lista-clientes">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 titulo-clientes">
                        <div class="col-md-6 titulo">
                            <p>QUEM<br>TAMBÉM<br>É ZERO<br>DEFECT</p>
                        </div>
                        <div class="col-md-6 logo-zero">
                            <img src="<?= base_url('assets/images/clientes.png')?>" class="img-responsive" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 clientes">
                        <div class="owl-clientes">
                            <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/01.jpg')?>)"></div>
                            <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/02.jpg')?>)"></div>
                            <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/03.jpg')?>)"></div>
                            <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/04.jpg')?>)"></div>
                            <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/05.jpg')?>)"></div>
                            <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/06.jpg')?>)"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--Contato-->
    <div class="section contato">
        <div class="container">
            <div class="col-md-7 titulo-contato">
                <p>ESTAMOS<br> ESPERANDO VOCÊ</p>
            </div>
            <div class="col-md-12 form-contato">
                <div class="row">
                    <form id="form-contato">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="nome" class="form-control" placeholder="NOME:" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="telefone" class="form-control telefone" placeholder="TELEFONE:" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="EMAIL:" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea name="mensagem" class="form-control" cols="75" rows="6" placeholder="COMO PODEMOS AJUDAR VOCÊ?" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <button class="button button--saqui button--inverted button--text-thick button--text-upper button--size-s  enviar-mensagem" data-text="ENVIAR MENSAGEM" data-loading-text="Enviando..." type="button" onclick="enviar_contato();">ENVIAR MENSAGEM</button>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 dados-zero">
                        <div class="row">
                            <div class="col-md-4">
                                <p class="contato"><?php echo $dados['telefone']; ?><br> <?= $dados['email']; ?></p>
                            </div>
                            <div class="col-md-4">
                                <p class="endereco"><?php echo $dados['endereco']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section localizacao">
        <div id="localizacao" class="mapa"></div>
        <div class="col-md-12 footer">
            <div class="col-md-4">
                <img class="pull-right img-responsive" src="<?php echo base_url('assets/images/logo.jpg'); ?>" alt="">
            </div>
            <div class="col-md-3"><p class="mapa-site pull-right">MAPA DO SITE<br><a href="#azero">A ZERO</a><br><a href="#solucoes">SOLUÇÕES</a><br><a href="#trabalhe-conosco" onclick="fbq('track', 'desejaTrabalherConosco');">TRABALHE CONOSCO</a><br><a href="#equipe">EQUIPE</a><br><a href="#clientes">CLIENTES</a><br><a href="#fale-conosco">FALE CONOSCO</a><br></div>
            <div class="col-md-4 direitos-singulo">
                <a href="http://www.singulo.com.br/">
                    <img class="pull-left img-responsive" src="<?php echo base_url('assets/images/logo_singulo.png'); ?>" alt="">
                </a>
                <br><p>TODOS OS DIREITOS RESERVADOS</p>
            </div>
        </div>
    </div>
</div>

<!--AnimatedMenuIcon -->
<?php $this->load->view('templates/footer',['appendScripts' => [
    base_url('assets/js/desktop.js')
]]); ?>

<script>
    /*$(document).ready(function(){
        $.fancybox({
            'href': $('#base_url').val() + 'assets/images/pop-up.jpg',
            //'iframe' : true,
            'closeClick': false,
            afterShow: function () {
                $('img.fancybox-image').on('click', function(){
                    window.location.href = '#fale-conosco';
                    $.fancybox.close();
                });
            }
        });
    });*/
</script>





