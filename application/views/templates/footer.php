</body>

<footer>
    <?php $this->load->view('templates/scripts'); ?>

    <?php if( isset( $appendScripts ) ) : ?>

        <?php foreach( $appendScripts as $script ) : ?>
            <script src="<?php echo $script; ?>"></script>
        <?php endforeach; ?>

    <?php endif; ?>
</footer>
</html>