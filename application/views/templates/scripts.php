<!--JQuery-->
<script src="<?php echo base_url('assets/plugins/jquery/jquery-2.2.0.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>

<!-- Alertify -->
<script src="<?php echo base_url('assets/plugins/alertify/js/alertify.js'); ?>"></script>

<!--Bootstrap-->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.js')?>"></script>

<!--Owl-carousel-->
<script src="<?php echo base_url('assets/plugins/owl-carousel/js/owl.carousel.js')?>"></script>

<!--FANCYBOX-->
<script src="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.js'); ?>"></script>
<!--<script src="--><?php //echo base_url('assets/plugins/fancybox/jquery.mousewheel-3.0.6.pack.js'); ?><!--"></script>-->

<!--Fullpage-->
<script src="<?php echo base_url('assets/plugins/fullpage/js/scrolloverflow.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/fullpage/js/jquery.fullpage.js'); ?>"></script>

<!--Modernizr-->
<script src="<?php echo base_url('assets/plugins/modernizr/js/modernizr.js'); ?>"></script>

<!--Modernizr-->
<script src="<?php echo base_url('assets/plugins/jsonify/jsonify.js'); ?>"></script>

<!--jqueryValidate-->
<script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.js'); ?>"></script>

<!--jqueryValidate-->
<script src="<?php echo base_url('assets/plugins/jquery.maskedinput/js/jquery.maskedinput.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js')?>"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAETycNvN9iZo18jvVePMhrIBmmGVrkmbU"></script>


