<!doctype html>
<html lang="pt-BR">

<head>
    <title>Testes para Mobile, Performance, E-commerce e Segurança - Zero-Defect</title>
    <meta name="description" content="ZERO DEFECT - Uma Empresa Pioneira Focada em Testes de Software. Trabalhamos com Testes de E-Commerce, Segurança e Mais! Conheça nossa Metodologia!">
    <meta name="keywords" content="testhouse ,software, segurança, test, performance">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <meta property="og:title" content="Zero-Defect">
    <meta property="og:image" content="<?= base_url('assets/images/logo.jpg'); ?>">
    <meta property="og:description" content="ZERO DEFECT - Uma Empresa Pioneira Focada em Testes de Software. Trabalhamos com Testes de E-Commerce, Segurança e Mais! Conheça nossa Metodologia!">

    <link rel="shortcut icon" href="<?php echo base_url('assets/images/favico.png'); ?>">

    <?php $this->load->view('templates/styles'); ?>

    <?php if( isset( $appendStyles ) ) : ?>

        <?php foreach( $appendStyles as $style ) : ?>
            <link href="<?php echo $style; ?>" rel="stylesheet">
        <?php endforeach; ?>

    <?php endif; ?>

    <input id="base_url" type="hidden" value="<?php echo base_url(); ?>">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TRQ792J');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1793753850933475');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1793753850933475&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->

    </head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TRQ792J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->