<form id="form-trabalhe-conosco">
    <div class="form-group">
        <input type="text" name="nome" class="form-control" placeholder="NOME:" required>
    </div>
    <div class="form-group">
        <input type="tel" name="telefone" class="form-control telefone" placeholder="TELEFONE:" required>
    </div>
    <div class="form-group">
        <input type="email" name="email" class="form-control" placeholder="EMAIL:" required>
    </div>
    <div class="form-group">
        <input type="text" name="sobre" class="form-control" placeholder="FALE UM POUCO SOBRE VOCÊ:" required>
    </div>
    <input type="file" data-filename-placement="inside" name="arquivo" title="ENVIAR ARQUIVO" accept=".txt, .pdf" required>
    <button data-text="ENVIAR CURRÍCULO" class="hidden-xs hidden-sm button button--saqui button--inverted button--text-thick button--text-upper button--size-s enviar-curriculo pull-right" data-loading-text="Enviando..." type="button" onclick="enviar_trabalhe_conosco(); ">ENVIAR CURRÍCULO</button>
    <div class="col-xs-12 hidden-md hidden-lg">
        <button data-text="ENVIAR CURRÍCULO" class="hidden-md hidden-lg button enviar-curriculo pull-right" data-loading-text="ENVIANDO..." type="button" onclick="enviar_trabalhe_conosco(); ">ENVIAR CURRÍCULO</button>
    </div>
</form>