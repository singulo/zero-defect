<!--Fonts-->

<link href="https://fonts.googleapis.com/css?family=Signika+Negative:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Baloo+Bhaina" rel="stylesheet">

<!-- Bootstrap CSS -->
<link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">

<!-- Font-Awesome -->
<link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

<!-- Owl-carousel -->
<link href="<?php echo base_url('assets/plugins/owl-carousel/css/owl.carousel.css'); ?>" rel="stylesheet">

<!-- FANCYBOX -->
<link href="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.css'); ?>" rel="stylesheet">

<!-- Alertify -->
<link href="<?php echo base_url('assets/plugins/alertify/css/alertify.css'); ?>" rel="stylesheet">

<!--FullPage-->
<link href="<?php echo base_url('assets/plugins/fullpage/css/jquery.fullpage.css'); ?>" rel="stylesheet">

<!--button-styles-inspiration-->
<link href="<?php echo base_url('assets/plugins/button-styles-inspiration/css/buttons.css'); ?>" rel="stylesheet">

<!--original-hover-effects-->
<link href="<?php echo base_url('assets/plugins/original-hover-effects/css/style_common.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/original-hover-effects/css/style10.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets/css/custom.css?v=1.3'); ?>" rel="stylesheet">


