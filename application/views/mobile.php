<?php $this->load->view('templates/head', ['appendStyles' => [
    base_url('assets/css/mobile.css')
]]); ?>

<?php
$dados = $this->config->item('zero-defect');
?>


<div class="loading" style="background: url(<?= base_url('assets/images/loading.gif');?>) center no-repeat #fff;" ></div>
<div id="mobile-fullpage">
<!--Home-->
    <div class="section home" style="background-image: url(<?= base_url('assets/images/home.jpg'); ?>); background-position: center;background-size: cover;">
        <div class="menu">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="<?= base_url('assets/images/logo-branco.png')?>" alt="logo-zero-defect">
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="#azero">A ZERO</a></li>
                            <li><a href="#solucoes">SOLUÇÔES</a></li>
                            <li><a href="#trabalhe-conosco">TRABALHE CONOSCO</a></li>
                            <li><a href="#equipe">EQUIPE</a></li>
                            <li><a href="#clientes">CLIENTES</a></li>
                            <li><a href="#fale-conosco">FALE CONOSCO</a></li>
                            <li><a href="#endereco">ENDEREÇO</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
        <div class="container">
            <div class="titulo-home">
                <p>NÓS SOMOS <br><span>A ZERO DEFECT</span></p>
            </div>
        </div>
    </div>
    <div class="section sobre">
        <div class="container">
            <img src="<?= base_url('assets/images/logo-roxo.png')?>" class="center-block" style="margin-bottom: 20px; width: 80%;">
            <p class="text-justify">Nós somos a Zero-Defect Test House. Estamos aqui por que acreditamos que podemos ir mais além. Um grupo de jovens cansados da mesmice focado em mudar o mundo oferecendo uma forma de garantir que os softwares dos nossos clientes sejam mais confiáveis e competitivos.</p>
            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fzdefect%2Fvideos%2F694452007411694&width=100%&show_text=false&height=200&appId" width="100%" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

        </div>
    </div>
    <div class="section lista-servicos">
        <div class="container">
            <div class="servicos">
                <?php foreach($textos['servicos'] as $servico) : ?>
                    <div class="item servico">
                        <img src="<?= $servico['img']; ?>" alt="<?= $servico['img_attr_alt']; ?>" class="img-responsive center-block">
                        <h2 class="text-center titulo"><?= $servico['tipo']; ?></h2>
<!--                        <p class="tipo-servico text-center">--><?//= $servico['tipo']; ?><!--</p>-->
                        <p class="descricao-servico text-justify center-block"><?= $servico['descricao']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="owl-controles-prev">
                <button type="button" class="owl-controle prev pull-left" onclick="owlServicos.trigger('prev.owl.carousel');" disabled>
                    <img src="<?= base_url('assets/images/back.png')?>" alt="" class="img-responsive  ">
                </button>
            </div>
            <div class="owl-controles-next">
                <button type="button" class="owl-controle next pull-right" onclick="owlServicos.trigger('next.owl.carousel');">
                    <img src="<?= base_url('assets/images/next.png')?>" alt="" class="img-responsive">
                </button>
            </div>
        </div>
    </div>

    <div class="section trabalhe-conosco">
        <div class="slide" style="background-image: url(<?= base_url('assets/images/trabalhe-conosco.jpg'); ?>); background-position: center;background-size: cover;">
            <div class="container">
                <div class="col-xs-offset-1 titulo-trabalhe-conosco">
                    <p>NÓS <br>ESTAMOS <br>BUSCANDO <br>O TALENTO <br>QUE HÁ <br>EM VOCÊ</p>
                </div>
                <div class="col-xs-offset-1 link-form">
                    <a onclick="$.fn.fullpage.moveSlideRight();">
                        <p>TRABALHE COM A GENTE   <img src="<?= base_url('assets/images/next-branco.png')?>" alt=""></p>
                    </a>
                </div>
            </div>
        </div>
        <div class="slide form-trabalhe-conosco">
            <div class="container">
                <div class="titulo-form-trabalhe-conosco">
                    <p>SE VOCÊ É UM JIRAYA OU UMA MAMBA NEGRA NO QUE FAZ, FALE COM A GENTE:</p>
                </div>
                <div class="form-curriculo">
                    <?php $this->load->view('templates/contato/form-trabalhe-conosco'); ?>
                </div>
            </div>
        </div>
    </div>
<!--Diretores-->
    <div class="section diretores">
        <div class="container">
            <div class="titulo-diretores">
                <p>NOSSOS DIRETORES</p>
            </div>
            <div class="lista-diretores">
                <?php foreach($textos['diretores'] as $diretor) : ?>
                    <div class="col-md-3 item diretor">
                        <img src="<?= $diretor['img']; ?>" class="img-responsive img-diretor center-block" alt="<?= $diretor['nome']; ?>">
                        <p class="nome text-center"><?= $diretor['nome']; ?></p>
                        <p class=" descricao text-justify center-block"><?= $diretor['descricao']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="owl-controles-prev">
                <button type="button" class="owl-controle prev pull-left" onclick="owlDiretores.trigger('prev.owl.carousel');" disabled>
                    <img src="<?= base_url('assets/images/back.png')?>" alt="" class="img-responsive  ">
                </button>
            </div>
            <div class="owl-controles-next">
                <button type="button" class="owl-controle next pull-right" onclick="owlDiretores.trigger('next.owl.carousel');">
                    <img src="<?= base_url('assets/images/next.png')?>" alt="" class="img-responsive">
                </button>
            </div>
        </div>
    </div>
    <!--Clientes-->
    <div class="section lista-clientes">
        <div class="container">
            <div class="titulo">
                <p>QUEM<br>TAMBÉM<br>É ZERO<br>DEFECT</p>
            </div>
            <div class="clientes">
                <div class="owl-clientes">
                    <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/01.jpg')?>)"></div>
                    <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/02.jpg')?>)"></div>
                    <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/03.jpg')?>)"></div>
                    <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/04.jpg')?>)"></div>
                    <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/05.jpg')?>)"></div>
                    <div class="item cliente" style="background-image: url(<?php echo base_url('assets/images/clientes/06.jpg')?>)"></div>
                </div>
            </div>
        </div>
    </div>
<!--Contato-->
    <div class="section contato">
        <div class="container">
            <div class="titulo-contato">
                <p>ESTAMOS TE <br>ESPERANDO</p>
            </div>
            <div class="form-contato">
                <form id="form-contato">
                    <div class="form-group">
                        <input type="text" name="nome" class="form-control" placeholder="NOME:" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" name="telefone" class="form-control telefone" placeholder="TELEFONE:" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="EMAIL:" required>
                    </div>

                    <textarea name="mensagem" class="form-control" cols="75" rows="5" placeholder="COMO PODEMOS AJUDAR VOCÊ?" required></textarea>

                    <button type="button" class="button button--saqui button--inverted button--text-thick button--text-upper button--size-s  enviar-mensagem" data-text="ENVIAR MENSAGEM" data-loading-text="Enviando..." onclick="enviar_contato(); ">ENVIAR MENSAGEM</button>
                </form>
            </div>
            <div class="dados-zero">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="contato"><?= $dados['telefone']; ?><br> <?= $dados['email']; ?></p>
                    </div>
                    <div class="col-xs-12">
                        <p class="endereco"><?= $dados['endereco']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section localizacao">
        <div class="footer">
            <div class="col-xs-6">
                <img class="center-block img-responsive" src="<?= base_url('assets/images/logo.jpg'); ?>" alt="">
            </div>
            <div class="col-xs-6" style="padding-top: 15px;">
                <div class="col-xs-6 text-center">
                    <a href="https://www.facebook.com/zdefect/" target="_blank">
                    <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x" style="color: #fff"></i>
                        <i class="fa fa-facebook fa-stack-1x fa-inverse" style="color: #660e7f;"></i>
                    </span>
                    </a>
                </div>
                <div class="col-xs-6 text-center">
                    <a href="https://www.instagram.com/zerodefecttesthouse/" target="_blank">
                    <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x" style="color: #fff"></i>
                        <i class="fa fa-instagram fa-stack-1x fa-inverse" style="color: #660e7f;"></i>
                    </span>
                    </a>
                </div>
            </div>
            <div class="col-xs-offset-3 col-xs-6"><p class="mapa-site pull-right">MAPA DO SITE<br><a href="#azero">A ZERO</a><br><a href="#solucoes">SOLUÇÕES</a><br><a href="#trabalhe-conosco">TRABALHE CONOSCO</a><br><a href="#equipe">EQUIPE</a><br><a href="#clientes">CLIENTES</a><br><a href="#fale-conosco">FALE CONOSCO</a><br></div>
            <div class="col-xs-12 direitos-singulo">
                <a href="http://www.singulo.com.br/">
                    <img class="center-block img-responsive" src="<?= base_url('assets/images/logo_singulo.png'); ?>" alt="">
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('templates/footer',['appendScripts' => [base_url('assets/js/mobile.js')]]); ?>
