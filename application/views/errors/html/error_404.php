<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>404 - Zero-Defect</title>
    <link href="https://fonts.googleapis.com/css?family=Signika+Negative:400,700" rel="stylesheet">
</head>
<body>
    <img src="/assets/images/erro-404.jpg" alt="Erro 404 Zero-Defect" style="margin-left: auto; margin-right: auto; display: block; margin-top: 15vh;">
    <a href="/" style="text-align: center; display: block; margin-top: 40px; color: #9c9c9c; font-size: 18px; text-decoration: none; font-family: 'Signika Negative', sans-serif;">Clique para navegar novamente em nosso site.</a>
</body>
</html>