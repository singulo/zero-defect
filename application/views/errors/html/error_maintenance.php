<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Zero-Defect</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Overpass" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        body
        {
            font-family: 'Overpass', sans-serif;
        }

        .frase
        {
            font-size: 2.9rem;
            line-height: 150px;
        }

        .frase,
        .antigo-site a,
        .contato
        {
            color: #fff;
        }

        body .container
        {
            padding-top: 15%;
        }

        .antigo-site
        {
            font-size: 1.9rem;
            border-right: 1px solid #fff;
            line-height: 100px;
        }

        .contato
        {
            border-left: 1px solid #fff;
            font-size: 1.4rem;
            padding-top: 1.3%;
        }

        .contato p.endereco
        {
            font-size: 1rem;
            line-height: 12px;
        }

        .contato,
        .antigo-site
        {
            height: 100px;
        }

        .rodape
        {
            position: absolute;
            left: 0;
            right: 0;
            bottom: 20px;
        }

        @media screen and (max-width: 1030px)
        {
            body .container
            {
                padding-top: 6%;
            }
        }
    </style>
</head>
<body style="background-image: url(<?php echo base_url('assets/images/novo-site/Fundo.jpg'); ?>); background-size: cover; background-position: center;">

<div class="container nova-zero">
    <div class="col-xs-12 col-md-3 frase text-center">AGUARDE UMA</div>
    <div class="col-xs-12 col-md-6">
        <img src="<?php echo base_url('assets/images/novo-site/Nova.png'); ?>" class="img-responsive center-block">
    </div>
    <div class="col-xs-12 col-md-3 frase text-center">ZERO DEFECT</div>
</div>

<div class="rodape">
    <div class="col-xs-6 antigo-site text-right">
        <a href="#">IR PARA O SITE</a>
    </div>
    <div class="col-xs-6 contato text-left">
        <p>51 <b>3012-2020</b>
            <br>
            <b>zero-defect@zero-defect</b>.com.br
        </p>
        <p class="endereco">Av. Julio de Castilhos 132 Salas 603/604<br>Centro Histórico - Porto Alegre - RS | CEP 90030-130</p>
    </div>
</div>

</body>
<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</footer>
</html>