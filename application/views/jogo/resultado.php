<!doctype html>
<html lang="pt-BR">

<head>
    <title>Zero-Defect - Quem você é na rede?</title>
    <meta name="keywords" content="testhouse ,software, segurança, test, performance">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <meta property="og:title" content="Meu resultado é: <?= ucwords(str_replace('_',' ',$heroi)); ?>">
    <meta property="og:image" content="<?= base_url('assets/images/jogo/' . $heroi . '_facebook.jpg')?>">
    <meta property="og:url" content="<?= base_url('jogo/resultado/'. $heroi)?>">
    <meta property="og:description" content="Venha responder nosso quiz e descobrir que personagem você é na rede...">

    <link rel="shortcut icon" href="<?php echo base_url('assets/images/favico.png'); ?>">

    <?php $this->load->view('templates/styles'); ?>

    <link href="<?php echo base_url('assets/pages/jogo/css/formulario.css'); ?>" rel="stylesheet">



    <input id="base_url" type="hidden" value="<?php echo base_url(); ?>">

</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="loading-resposta-jogo" style="background: url(<?= base_url('assets/images/loading.gif');?>) center no-repeat #fff;" ></div>

<section id="resultado" class="resultado" style="background-image: url(<?= base_url('assets/images/jogo/' . $heroi . '.jpg')?>)">
    <div style="position: absolute; bottom: 75px; left: 0; right: 0; margin-left: auto; margin-right: auto; display: block; width: 350px;" class="fb-share-button" data-layout="button" data-size="large" data-href="<?= base_url('jogo/resultado/'. $heroi)?>"></div>
</section>



<?php $this->load->view('templates/footer',['appendScripts' => [
    base_url('assets/plugins/bgloaded/bg-loaded.js'),
    base_url('assets/pages/jogo/js/formulario.js'),
    base_url('assets/pages/jogo/js/resultado.js')
]]); ?>