<?php $this->load->view('templates/head', ['appendStyles' => [
    base_url('assets/pages/jogo/css/formulario.css')
]]); ?>

<div class="loading" style="background: url(<?= base_url('assets/images/loading.gif');?>) center no-repeat #fff;" ></div>

<form id="form-pergunta">
    <h1 style="display: none;"></h1>
    <section class="inicio" style="background-image: url(<?= base_url('assets/images/jogo/background_inicio.jpg')?>)">
        <div class="container">
           <div class="col-md-12 pergunta">
                <button type="button" class="btn btn-default center-block" onclick="$('#form-pergunta').steps('next');">Iniciar</button>
           </div>
        </div>
    </section>
    <?php foreach ($perguntas as $key => $pergunta) :?>
        <h1 style="display: none;"></h1>
        <section id="pergunta" class="pergunta <?= (($key + 1) % 2 == 0)?  'par' :  'impar'; ?>">
            <div class="container">
                <div class="col-md-2">
                    <p class="numero-pergunta"><?= ($key + 1)?></p>
                </div>
                <div class="col-md-7 texto-pergunta">
                    <p class="<?= (strlen($pergunta['pergunta'] <= 49)) ? 'texto-pequeno' : ''; ?> "><?= $pergunta['pergunta']; ?></p>
                </div>
                <div class="col-md-10 col-md-offset-2">
                    <?php foreach($pergunta['alternativas'] as $valor => $alternativa) : ?>
                        <label class="opcao-resposta"><input type="radio" name="<?= $pergunta['nome']; ?>" value="<?= $valor; ?>" data-ultimo="<?= ( isset( $pergunta['ultima-pergunta']) && $pergunta['ultima-pergunta'] === true ) ? 'true' : 'false'; ?>"> <?= $alternativa; ?></label><br>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    <?php endforeach; ?>
</form>

<?php $this->load->view('templates/footer',['appendScripts' => [
    base_url('assets/plugins/jquery-steps/js/jquery.steps.js'),
    base_url('assets/plugins/bgloaded/bg-loaded.js'),
    base_url('assets/pages/jogo/js/formulario.js')
]]); ?>



